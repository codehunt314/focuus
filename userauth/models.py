from django.db import models

from django.db import models
from django.db.models.signals import post_save
from django.contrib.auth.models import User
from twitterauth import TWITTER_CONSUMER_KEY, TWITTER_CONSUMER_SECRET, TWITTER_CALLBACK, req_token_url, authorize_url, access_token_url
import urllib, urlparse, time, oauth, json
from datetime import datetime
from dateutil import parser

class UserProfile(models.Model):
    user                =   models.ForeignKey(User, blank=True, null=True)
    twitterid           =   models.CharField(max_length=255, blank=True, null=True, editable=False)
    screen_name         =   models.CharField(max_length=255, blank=True, null=True, editable=False)
    access_token        =   models.CharField(max_length=255, blank=True, null=True, editable=False)
    access_token_secret =   models.CharField(max_length=255, blank=True, null=True, editable=False)
    profile_image_url   =   models.URLField(blank=True, null=True)
    location            =   models.CharField(max_length=100, blank=True, null=True)
    website             =   models.URLField(blank=True, null=True)
    description         =   models.CharField(max_length=160, blank=True, null=True)
    since_id            =   models.CharField(max_length=255, blank=True, null=True)
    max_id              =   models.CharField(max_length=255, blank=True, null=True)
    max_prcessed_id     =   models.CharField(max_length=255, default='0')
 
    def __str__(self):
        return "%s's profile" % self.user
 
    def get_home_timeline(self, max_id=None, count=200):
        if self.user:
            timeline_url    =   'https://api.twitter.com/1.1/statuses/home_timeline.json'
            access_token    =   {'oauth_token': self.access_token, 'oauth_token_secret': self.access_token_secret}
        else:
            timeline_url    =   'https://api.twitter.com/1.1/statuses/user_timeline.json'
            access_token    =   {'oauth_token': '22993288-kNMIm2Qj4nrkOdQ4nrbEJ1Wznk8CijxpMtgfNK5M0', 'oauth_token_secret': 'MuTx77rc8mzgqvNwklY5J8vczcIgw6LivYMdYgiEKaqn0'}
        params              =   {
                                    'oauth_consumer_key'    :   TWITTER_CONSUMER_KEY,
                                    'oauth_nonce'           :   oauth.generate_nonce(),
                                    'oauth_signature_method':   "HMAC-SHA1",
                                    'oauth_timestamp'       :   str(int(time.time())),
                                    'oauth_token'           :   access_token.get('oauth_token'),
                                    'include_rts'           :   1,
                                    'screen_name'           :   self.screen_name,
                                    'count'                 :   count
                                }
        
        if self.since_id:
            params['since_id']  =   int(self.since_id)
        if self.max_id:
            params['max_id']    =   max_id
        req             =   oauth.OAuthRequest(http_method="GET", http_url=timeline_url, parameters=params)
        consumer        =   oauth.OAuthConsumer(key=TWITTER_CONSUMER_KEY, secret=TWITTER_CONSUMER_SECRET)
        token           =   oauth.OAuthToken(access_token.get('oauth_token'), access_token.get('oauth_token_secret'))
        signature       =   oauth.OAuthSignatureMethod_HMAC_SHA1().build_signature(req,consumer,token)
        req.set_parameter('oauth_signature', signature)
        content         =   json.loads(urllib.urlopen(req.to_url()).read())
        new_urls        =   []
        from news.models import NewsViaFriends
        for c in content:
            for e in c.get('entities', {}).get('urls', []):
                try:
                    new_urls.append(e.get('expanded_url'))
                    news_item, created  =   NewsViaFriends.objects.get_or_create(
                        twitter_newsurl =   e.get('expanded_url'),
                        userprofile     =   self,
                        published_time  =   parser.parse(c.get('created_at')),
                    )
                    news_item.fetch_summary()
                except:
                    print e
        idlist   =   [c['id'] for c in content]
        # idlist.sort()
        if len(idlist)>0:
            if not self.max_id:
                self.max_id         =   min(idlist) - 1
            else:
                next_max_id         =   min(idlist) - 1
                if next_max_id < self.max_id:
                    self.max_id     =   next_max_id
                else:
                    pass
            if not self.since_id:
                self.since_id       =   max(idlist)
            else:
                next_since_id       =   max(idlist)
                if next_since_id > self.since_id:
                    self.since_id   =   next_since_id
                else:
                    pass
            self.save()
        else:
            self.max_id     =   None
            self.save()
        return new_urls


def create_user_profile(sender, instance, created, **kwargs):
    if created:
        profile, created = UserProfile.objects.get_or_create(user=instance)
 
post_save.connect(create_user_profile, sender=User)

