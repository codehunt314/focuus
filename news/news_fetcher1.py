import os, sys

PROJECT_PATH = os.path.dirname(os.path.realpath(__file__))
sys.path.append(os.path.join(PROJECT_PATH, '../'))
os.environ['DJANGO_SETTINGS_MODULE'] = 'focuus.settings'

import gevent.monkey
gevent.monkey.patch_socket()
gevent.monkey.patch_ssl()
from gevent.pool import Pool
import requests
import logging
logging.getLogger("requests").setLevel(logging.WARNING)
from readability.readability import Document
from datetime import datetime

from news.models import *
urls = [(i.id, i.twitter_newsurl) for i in NewsViaFriends.objects.filter(fetched=False) if i.twitter_newsurl is not None]
results = []

def fetch(param):
	try:
		headers = 	{'User-agent': 'Mozilla/5.0 (Windows; U; Windows NT 5.1; de; rv:1.9.1.5) Gecko/20091102 Firefox/3.5.5'}
		r 		= 	requests.get(param[1], headers=headers, verify=False)
		html 	=	r.content
		title 	=	Document(html).short_title() if html!='' else ''
		results.append((param[0], title, r.url))
		print title
		#print "URL: %s" % (url)
	except:
		pass

pool 	= 	Pool(2000)
for newsitemid,url in urls[17:]:
	pool.spawn(fetch, (newsitemid, url))

start 	= 	datetime.now()
pool.join()
print (datetime.now() - start).seconds

for pk,title,url in results:
	news_item = NewsViaFriends.objects.get(id=pk)
	news_item.newsurl_title = 	title
	news_item.expanded_newsurl 	= 	url
	news_item.save()



