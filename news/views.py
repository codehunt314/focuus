from django.http import HttpResponseRedirect, HttpResponse
from django.shortcuts import render, render_to_response
from django.template import RequestContext, loader
from django.utils.translation import ugettext, ugettext_lazy as _
from django.contrib.auth import get_user
from django.views.decorators.csrf import csrf_exempt
from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger
from django.core.mail import send_mail, BadHeaderError

import string, random
import json, random, urllib2
from datetime import datetime, timedelta
from bs4 import BeautifulSoup
from urlparse import urlparse
from news.models import *


def id_generator(size=10, chars=string.ascii_uppercase + string.digits):
    return ''.join(random.choice(chars) for _ in range(size))


def _get_summary(news_item):
    soup = BeautifulSoup(news_item.newsurl_summary)
    text = [(len(p.text), p.text) for p in soup.findAll('p')]
    text.sort()
    ret_summary = None
    if len(text)>0:
        if len(text[-1][1])>80:
            ret_summary = text[-1][1][:500]
    images = []
    for img in soup.findAll('img'):
        for v in img.attrs.values():
            if type(v) in [str, unicode]:
                temp = v.rsplit('.')
                if len(temp)>0 and temp[-1] in ['png', 'jpg', 'jpeg', 'bmp', 'gif']:
                    if v.startswith('http:') or v.startswith('https:'):
                        print v
                        images.append(v)
                    else:
                        parsed_uri = urlparse(news_item.expanded_newsurl)
                        domain = '{uri.scheme}://{uri.netloc}'.format(uri=parsed_uri)
                        print domain+v
                        images.append(domain+v)
    if len(images)>0:
        ret_img = images[0]
    else:
        ret_img = None
    return ret_summary, ret_img

def new_home(request):
    current     =   datetime.strptime(request.GET.get('d', None), "%d%m%Y").date() if request.GET.get('d', None) else datetime.today().date()
    prev_day    =   current - timedelta(1)
    next_day    =   None if current==datetime.today().date() else current + timedelta(1)
    news_list   =   NewsViaFriends.objects.filter(
                        published_time__year    =   current.year,
                        published_time__month   =   current.month,
                        published_time__day     =   current.day
                    ).exclude(expanded_newsurl=None)#.order_by('-retweet_count')
    # import pdb; pdb.set_trace()
    paginator   =   Paginator(news_list, 100)
    page        =   request.GET.get('page')
    try:
        news    =   paginator.page(page)
    except PageNotAnInteger:
        news    =   paginator.page(1)
    except EmptyPage:
        news    =   paginator.page(paginator.num_pages)

    ret_news    =   []
    for news_item in news:
        # news_item.fetch_summary()
        if news_item.expanded_newsurl:
            ret_news.append(
                {
                    'id':   news_item.id,
                    'title': news_item.newsurl_title, 
                    'url': news_item.expanded_newsurl, 
                    'source': urlparse.urlparse(news_item.expanded_newsurl).netloc, 
                    'retweet_count': news_item.retweet_count,
                    'status': news_item.status
                }
            )
    return render_to_response('index.html', 
        {'news': ret_news, 'current': current, 'next_day': next_day, 'prev_day': prev_day, 'is_staff': request.user.is_staff}, 
        context_instance=RequestContext(request)
    )


def subscribe(request):
    if request.method=='POST':
        email = request.POST.get('email')
        import pdb; pdb.set_trace()
        if email:
            # email_subs, created =   EmailSubscriber.objects.get_or_create(email=email, subs_code=id_generator())
            mailchimp_client.lists.subscribe('65d2595dfa', {'email': email})
            # send_mail('Newz: Please Confirm Subscription', 'Newz: Please Confirm Subscription', 'vipul.shaily@gmail.com', [email], html_message=loader.render_to_string('welcome.html'))
            return HttpResponse('Thank you subscribing!!')
    return HttpResponse('hello')

def verify_subscription(request):
    try:
        # email_subs  =   EmailSubscriber.objects.get(subs_code=request.GET.get(email), email=request.GET.get(email))
        # email_subs.verified = True
        # email_subs.save()
        return HttpResponse('Subscription verified, daily news article delivered to you in morning between 8AM-9AM')
    except:
        return HttpResponse('Some error')

def unsubscribe(request):
    try:
        # email_subs          =   EmailSubscriber.objects.get(subs_code=request.GET.get(email), email=request.GET.get(email))
        # email_subs.status   =   'UNSUBSCRIBED'
        # email_subs.save()
        mailchimp_client.lists.unsubscribe('65d2595dfa', {'email': request.GET.get('email')})
        return HttpResponse('Thanks for having you, we would like to know your feedback, please email me vipul.shaily@gmail.com')
    except:
        return HttpResponse('Some error')


def set_status(request):
    if request.method=='POST':
        newsitemid  =   request.POST.get('newsitemid')
        status      =   request.POST.get('status')
        news_item   =   NewsViaFriends.objects.get(id=int(newsitemid))
        news_item.status = status
        news_item.save()
    return HttpResponseRedirect(request.META.get('HTTP_REFERER', '/'))

