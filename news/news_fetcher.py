import os, sys
from dateutil import parser
import twitter


PROJECT_PATH = os.path.dirname(os.path.realpath(__file__))
sys.path.append(os.path.join(PROJECT_PATH, '../'))
os.environ['DJANGO_SETTINGS_MODULE'] = 'focuus.settings'

from news.models import *
from userauth.twitterauth import TWITTER_CONSUMER_KEY, TWITTER_CONSUMER_SECRET
from userauth.models import UserProfile

api 	=	twitter.Api(consumer_key=TWITTER_CONSUMER_KEY,
				consumer_secret=TWITTER_CONSUMER_SECRET,
				access_token_key='22993288-kNMIm2Qj4nrkOdQ4nrbEJ1Wznk8CijxpMtgfNK5M0',
				access_token_secret='MuTx77rc8mzgqvNwklY5J8vczcIgw6LivYMdYgiEKaqn0'
			)

new_urls = []

def get_user_timeline(screen_name):
	user_profile, created   =   UserProfile.objects.get_or_create(screen_name=screen_name)
	since_id 		= 	user_profile.since_id
	max_id 			= 	user_profile.max_id
	total_count 	= 	0
	fetch_url_count = 	0
	while True:
		statuses 	= 	api.GetUserTimeline(screen_name=user_profile.screen_name, max_id=max_id, count=200, include_rts=True)
		total_count += len(statuses)
		idlist 		=	[]
		for item in statuses:
			idlist.append(item.id)
			fetch_url_count += len(item.urls)
			for each_url in item.urls:
				news_item, created 	= 	NewsViaFriends.objects.get_or_create(
												twitter_newsurl =   each_url.expanded_url,
												defaults 		=	{
													'userprofile' 	:	user_profile,
													'published_time': 	parser.parse(item.created_at),
													'retweet_count' : 	item.GetRetweetCount()
												}
											)
				if not news_item.fetched:
					new_urls.append((news_item.id, news_item.twitter_newsurl))
				# news_item.fetch_summary()
		if len(idlist)>0:
			max_id 		=	min(idlist) - 1
			since_id 	=	max(max(idlist), since_id)
		else:
			pass
		user_profile.since_id =	since_id
		user_profile.save()
		break
	print 'Processed %s tweets' % (total_count)
	print 'Fetched %s urls' % (fetch_url_count)
	return True



import gevent.monkey
gevent.monkey.patch_socket()
gevent.monkey.patch_ssl()
from gevent.pool import Pool
import requests
import logging
logging.getLogger("requests").setLevel(logging.WARNING)
from readability.readability import Document
from datetime import datetime
results = []

def fetch(param):
	try:
		headers = 	{'User-agent': 'Mozilla/5.0 (Windows; U; Windows NT 5.1; de; rv:1.9.1.5) Gecko/20091102 Firefox/3.5.5'}
		r 		= 	requests.get(param[1], headers=headers, verify=False)
		html 	=	r.content
		title 	=	Document(html).short_title() if html!='' else ''
		results.append((param[0], title, r.url))
		print title
		#print "URL: %s" % (url)
	except:
		pass


def main():
	for fuzzyc in FuzzyCategory.objects.all():
		try:
			get_user_timeline(fuzzyc.userprofile.screen_name)
		except Exception, e:
			print e
			pass
	pool 	= 	Pool(len(new_urls))
	for newsitemid,url in new_urls:
		pool.spawn(fetch, (newsitemid, url))
	pool.join()
	print len(results)
	for pk,title,url in results:
		try:
			news_item = NewsViaFriends.objects.get(id=pk)
			news_item.newsurl_title 	= 	title
			news_item.expanded_newsurl 	= 	url
			news_item.fetched 			=	True
			news_item.save()
		except:
			print pk,title,url
			pass

if __name__ == "__main__":
	print '\n\n============Fetching Data============\n\n'
	main()


