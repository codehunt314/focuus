from django.db import models
from userauth.models import *
import urllib2, urllib, cookielib
from readability.readability import Document
import requests


class NewsViaFriends(models.Model):
	userprofile 		=	models.ForeignKey(UserProfile, blank=True, null=True)
	twitter_newsurl 	=	models.TextField()
	expanded_newsurl 	=	models.TextField(blank=True, null=True)
	newsurl_title 		=	models.TextField(blank=True, null=True)
	newsurl_summary		=	models.TextField(blank=True, null=True)
	newsurl_source 		=	models.CharField(max_length=255, blank=True, null=True)
	published_time 		=	models.DateTimeField(auto_now=False, auto_now_add=False)
	newsurl_author 		=	models.CharField(max_length=255, blank=True, null=True)
	newsurl_author_twitterid 	=	models.CharField(max_length=255, blank=True, null=True)
	fetched 			=	models.BooleanField(default=False)
	retweet_count 		=	models.IntegerField(default=0)
	# sticky 				=	models.BooleanField(default=False)
	status 				=	models.CharField(max_length=255, default='DISCOVERED', choices=(('DISCOVERED', 'DISCOVERED'), ('SELECTED', 'SELECTED'), ('STICKY', 'STICKY')))


	def fetch_summary(self):
		if self.fetched:
			return True
		headers = {'User-agent': 'Mozilla/5.0 (Windows; U; Windows NT 5.1; de; rv:1.9.1.5) Gecko/20091102 Firefox/3.5.5'}
		try:
			# u 	=	opener.open(self.twitter_newsurl)
			r 	= 	requests.get(self.twitter_newsurl, headers=headers)
		except Exception, e:
			r 	= 	None
			print e
			print 'ERROR:\t%s'%(self.twitter_newsurl) 
		if not r:
			return False
		html 	= 	r.content
		if html=='':
			html = 	requests.get(r.url, headers=headers).content
		self.expanded_newsurl 	= 	r.url
		self.newsurl_title 		= 	Document(html).short_title() if html!='' else ''
		# self.newsurl_summary 	= 	Document(html).summary() if html!='' else ''
		self.fetched 			=	True
		self.save()
		return True


class Category(models.Model):
	name 		=	models.CharField(max_length=255, default='Generic')

class FuzzyCategory(models.Model):
	category 	=	models.CharField(max_length=255, default='Generic')
	userprofile =	models.ForeignKey(UserProfile, blank=True, null=True)


def populate_fuzzyCategory():
	data = {
				'News & Opinion': [
						'sgurumurthy','TheJaggi','minhazmerchant','swapan55', 'ShekharGupta','arvindsubraman','SwarajyaMag', 
						'ARangarajan1972', 'toralvaria', 'pbmehta', 'RMantri', 'barugaru', 'kaushikcbasu', 'mediacrooks', 
						'dhume', 'bibekdebroy', 'Jairam_Ramesh', 'praveenswami', 'MohanCRaja'
					]
	}
	for k,v in data.items():
		for user_name in v:
			user_profile, created   =   UserProfile.objects.get_or_create(screen_name=user_name)
			obj, created 			=	FuzzyCategory.objects.get_or_create(category=k, userprofile=user_profile)


class EmailSubscriber(models.Model):
	email 		=	models.EmailField()
	subs_code	=	models.CharField(max_length=255)
	verified	=	models.BooleanField(default=False)
	created_at 	= 	models.DateTimeField(auto_now=True, auto_now_add=True)
	varified_at = 	models.DateTimeField(auto_now=True, auto_now_add=False)
	status 		=	models.CharField(max_length=255, default='SUBSCRIBED')


class NewsLetter(models.Model):
	subscriber 	=	models.ForeignKey(EmailSubscriber)
	msg 		=	models.TextField()
	subject 	=	models.TextField()
	from_email	=	models.EmailField()
	sent 		=	models.BooleanField(default=False)
	status 		=	models.CharField(max_length=255, choices=(('sent', 'sent'), ('inqueue', 'inqueue'), ('failed', 'failed')))
	sent_time 	=	models.DateTimeField(auto_now=True, auto_now_add=True)

