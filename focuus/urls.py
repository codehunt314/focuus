from django.conf.urls import patterns, include, url

from settings import MEDIA_ROOT
# Uncomment the next two lines to enable the admin:
# from django.contrib import admin
# admin.autodiscover()

urlpatterns = patterns('',
    # url(r'^$', 'opinions.views.home', name='home'),
    url(r'^$', 'news.views.new_home', name='new_home'),
    url(r'^subscribe/$', 'news.views.subscribe', name='subscribe'),
    url(r'^unsubscribe/$', 'news.views.unsubscribe', name='unsubscribe'),
    url(r'^verify_subscription/$', 'news.views.verify_subscription', name='verify_subscription'),
    url(r'^set_status/$', 'news.views.set_status', name='set_status'),
    url(r'^admin_login/$', 'userauth.views.admin_login', name='admin_login'),
    url(r'^admin_logout/$', 'userauth.views.admin_logout', name='admin_logout'),

    # url(r'^next/$', 'opinions.views.getarticle_ajax', name='getarticle_ajax'),
    # url(r'^next/$', 'opinions.views.get_articles', name='get_articles'),
    # url(r'^upvote/$', 'opinions.views.upvote', name='upvote'),
    # url(r'^comment/(?P<articleid>\d+)/$', 'opinions.views.comment', name='comment'),
    # url(r'^joinus/$', 'userauth.views.joinus', name='joinus'),
    # url(r'^signout/$', 'userauth.views.signout', name='signout'),
    # url(r'^complete/twitter/$', 'userauth.views.callback', name='callback'),
    url('^site_media/(?P<path>.*)$', 'django.views.static.serve', {'document_root':MEDIA_ROOT})
)
