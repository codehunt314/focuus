"""import urllib
from bs4 import BeautifulSoup

#times of india
url = 'http://blogs.timesofindia.indiatimes.com/'
soup = BeautifulSoup(urllib.urlopen(url).read())

post = soup.findAll('div', attrs={'class': 'media-body'})
for ep in post:
    datentime = ep.find('div', attrs={'class':'media-meta'}).find('span').text
    authored = ep.find('div', attrs={'class':'media-meta'}).find('a').text
    post_heading = ep.find(attrs={'class':'media-heading'}).text
    print datentime, authored, post_heading

"""

#indian express
#http://indianexpress.com/section/opinion/feed/
#http://indianexpress.com/section/opinion/columns/
#http://indianexpress.com/section/opinion/editorials/


#new indianexpress
#http://www.newindianexpress.com/columns/


#the hindu
#http://www.thehindu.com/archive/web/2014/07/01/

#hindustantimes
#http://www.hindustantimes.com/comment/


######################################################
######################################################

from opinions.models import *

{'typeofsource': 'newspaper', 'name': '', 'website': '', 'feeds': []}
data = [
    {'typeofsource': 'newspaper', 'name': 'Asian Age', 'website': 'asianage.com',                     'feeds': [('http://www.asianage.com/more/editorial', 'custom'), ('http://www.asianage.com/rss/79', 'rss'), ('http://www.asianage.com/rss/212', 'rss')]},
    {'typeofsource': 'newspaper', 'name': 'Business Line', 'website': 'thehindubusinessline.com',     'feeds': [('http://www.thehindubusinessline.com/opinion/', 'custom'), ('http://www.thehindubusinessline.com/features/?service=rss', 'rss'), ('http://www.thehindubusinessline.com/opinion/?service=rss', 'rss')]},
    {'typeofsource': 'newspaper', 'name': 'Business Standard', 'website': 'business-standard.com',     'feeds': [('http://www.business-standard.com/rss/opinion-columns-1050201.rss', 'rss')]},
    {'typeofsource': 'newspaper', 'name': 'Deccan Chronicle', 'website': 'deccanchronicle.com',     'feeds': [('http://www.deccanchronicle.com/commentary/columnists', 'custom')]},
    {'typeofsource': 'newspaper', 'name': 'Deccan Herald', 'website': 'deccanherald.com',             'feeds': [('http://www.deccanherald.com/rss/opinion.rss', 'rss'), ('http://www.deccanherald.com/rss/columns.rss', 'rss'), ('http://www.deccanherald.com/rss/analysis.rss', 'rss')]},
    {'typeofsource': 'newspaper', 'name': 'Freepress Journal', 'website': 'freepressjournal.in',     'feeds': [('http://freepressjournal.in/category/edit/feed/', 'rss')]},
    {'typeofsource': 'newspaper', 'name': 'Mid Day', 'website': 'mid-day.com',                         'feeds': [('http://www.mid-day.com/Resources/midday/rss/news-columnists.xml', 'rss')]},
    {'typeofsource': 'newspaper', 'name': 'Live Mint', 'website': 'livemint.com',                     'feeds': [('http://www.livemint.com/rss/opinion', 'rss')]},
    {'typeofsource': 'newspaper', 'name': 'Mumbai Mirror', 'website': 'mumbaimirror.com',             'feeds': [('http://www.mumbaimirror.com/columns/articlelist/17372378.cms', 'custom')]},
    {'typeofsource': 'newspaper', 'name': 'Daily Pioneer', 'website': 'dailypioneer.com',             'feeds': [('http://www.dailypioneer.com/columnists', 'custom')]},
    {'typeofsource': 'newspaper', 'name': 'The Hindu', 'website': 'the.com',                         'feeds': [('http://www.thehindu.com/opinion/?service=rss', 'rss'), ('http://www.thehindu.com/archive/web/2014/07/01/', 'rss')]},
    {'typeofsource': 'newspaper', 'name': 'Hindustan Times', 'website': 'hindustantimes.com',         'feeds': [('http://feeds.hindustantimes.com/HT-Analysis', 'rss'), ('http://feeds.hindustantimes.com/HT-Analysis?format=xml', 'rss')]},
    {'typeofsource': 'newspaper', 'name': 'Indian Express', 'website': 'indianexpress.com',         'feeds': [('http://indianexpress.com/section/opinion/editorials/feed/', 'rss'), ('http://indianexpress.com/section/opinion/feed/', 'rss')]},
    {'typeofsource': 'newspaper', 'name': 'New Indian Express', 'website': 'newindianexpress.com',     'feeds': [('http://www.newindianexpress.com/editorials/?widgetName=rssfeed&widgetId=276210&getXmlFeed=true', 'rss'), ('http://www.newindianexpress.com/columns/?widgetName=rssfeed&widgetId=276210&getXmlFeed=true', 'rss')]},
    {'typeofsource': 'newspaper', 'name': 'Telegraph', 'website': 'telegraphindia.com',             'feeds': [('http://www.telegraphindia.com/feeds/rss.jsp?id=6', 'rss')]},
    {'typeofsource': 'newspaper', 'name': 'The Tribune', 'website': 'tribuneindia.com',             'feeds': [('http://www.tribuneindia.com/2014/20140712/edit.htm', 'custom')]},
    {'typeofsource': 'newspaper', 'name': 'The Economic Times', 'website': 'economictimes.indiatimes.com', 'feeds': [('http://economictimes.indiatimes.com/opinion/opinionshome/rssfeeds/897228639.cms', 'rss'), ('http://economictimes.indiatimes.com/Opinion/Editorial/rssfeeds/3376910.cms', 'rss'), ('http://blogs.economictimes.indiatimes.com/main/feed/entries/rss', 'rss')]},
    {'typeofsource': 'magazine', 'name': 'India Today', 'website': 'indiatoday.intoday.in',         'feeds': [('http://indiatoday.feedsportal.com/c/33614/f/647964/index.rss', 'rss'), ('http://indiatoday.feedsportal.com/c/33614/f/589701/index.rss', 'rss')]}
]

for source in data:
    source, created = Source.objects.get_or_create(title=source['name'], website=source['website'], type_of_source=source['typeofsource'])


for source in data:
    source_obj = Source.objects.get(title=source['name'])
    for fd in source['feeds']:
        SourceFeedInfo.objects.get_or_create(source=source_obj, feedurl=fd[0], feedfetchertype=fd[1])



from opinions.models import *
for i in SourceFeedInfo.objects.filter(feedfetchertype='rss'):
    i.fetch_feed()


#pip install redis requests readability-lxml
import requests
from readability.readability import Document

def fetch_content(url):
    response = requests.get(url, timeout=10)
    if response.ok:
        page_content = Document(response.content).summary()
        return page_content
    return None

url = 'http://www.firstpost.com/politics/first-2g-now-katju-expose-is-dmk-the-worst-political-ally-ever-1629119.html'
fetch_content(url)

#http://hameedullah.com/step-by-step-guide-to-use-sign-in-with-twitter-with-django.html


import urllib
from bs4 import BeautifulSoup

def author(url):
    authors = []
    soup = BeautifulSoup(urllib.urlopen(url).read())
    for i in soup.findAll():
        cls = i.attrs.get('class', [])
        for c in cls:
            if c.find('author')!=-1:
                authors.append(i.text)
        ids = i.attrs.get('id', '')
        if ids.find('author')!=-1:
            authors.append(i.text)
    return filter(lambda x: len(x)<30, authors)

def pubdate(url):
    pubdates = []
    soup = BeautifulSoup(urllib.urlopen(url).read())
    for i in soup.findAll():
        cls = i.attrs.get('class', [])
        for c in cls:
            if c.find('date')!=-1:
                pubdates.append(i.text)
        ids = i.attrs.get('id', '')
        if ids.find('date')!=-1:
            pubdates.append(i.text)
    return pubdates

