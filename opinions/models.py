import feedparser
from time import mktime
from datetime import datetime

from django.db import models
import requests
from readability.readability import Document

from userauth.models import *


TYPES_OF_SOURCE     =     (
                            ('magazine', 'magazine'), 
                            ('newspaper', 'newspaper'), 
                            ('blog', 'blog'), 
                            ('online news site', 'online news site'),
                            ('twitter', 'twitter'),
                            ('others', 'others'),
                    )

FEED_TYPE     =     (
                    ('rss', 'rss'),
                    ('twitter', 'twitter'),
                    ('custom', 'custom')        
            )

class Source(models.Model):
    title             =     models.CharField(max_length=255, unique=True)
    website         =     models.CharField(max_length=255, unique=True)
    type_of_source     =    models.CharField(max_length=255, choices=TYPES_OF_SOURCE)
    other_details    =    models.TextField(blank=True, null=True)


class SourceFeedInfo(models.Model):
    source          =   models.ForeignKey(Source)
    feedurl         =   models.CharField(max_length=255)
    feedfetchertype =   models.CharField(max_length=255, choices=FEED_TYPE)
    lastfetched     =   models.DateTimeField(auto_now=True, auto_now_add=True)
    other_details   =   models.TextField(blank=True, null=True)

    def fetch_feed(self, verbose=True):
        if self.feedfetchertype=='rss':
            feed_result        =     feedparser.parse(self.feedurl)
            for entry in feed_result.get('entries', []):
                entry_title     =   entry.get('title')
                entry_url       =   entry.get('link')
                entry_summary   =   entry.get('summary')
                entry_pubtime   =   datetime.fromtimestamp(mktime(entry.get('published_parsed'))) if entry.get('published_parsed') else None
                try:
                    article, created    =   Article.objects.get_or_create(
                                                url         =    entry_url, 
                                                defaults    =    {
                                                                    'title'         : entry_title, 
                                                                    'summary'       : entry_summary,
                                                                    'published_time': entry_pubtime,
                                                                    'source'        : self.source,
                                                                    'source_name'   : self.source.title,
                                                                    'source_website': self.source.website
                                                                }
                                            )
                    if verbose and created:
                        print entry_title, entry_pubtime
                except:
                    print self.feedurl
                    pass


class Article(models.Model):
    url             =   models.CharField(max_length=255)
    title           =   models.CharField(max_length=255)
    summary         =   models.TextField(blank=True, null=True)
    author          =   models.ForeignKey(User, blank=True, null=True)
    author_name     =   models.CharField(max_length=255, blank=True, null=True)
    author_twitter  =   models.CharField(max_length=255, blank=True, null=True)
    source          =   models.ForeignKey(Source)
    source_name     =   models.CharField(max_length=255, blank=True, null=True)
    source_website  =   models.CharField(max_length=255, blank=True, null=True)
    published_time  =   models.DateTimeField(auto_now=False, auto_now_add=False, blank=True, null=True)

    def fetch_summary(self, force=False):
        if not self.summary or force:
            response = requests.get(self.url, timeout=10)
            if response.ok:
                self.summary = Document(response.content).summary()
                self.save()
        return self.summary


class ArticleVote(models.Model):
    article     =   models.ForeignKey(Article)
    voted_by    =   models.ForeignKey(User, default=None, blank=True)
    voted_at    =   models.DateField(auto_now=True, auto_now_add=True)


class ArticleComment(models.Model):
    article         =   models.ForeignKey(Article)
    comment         =   models.TextField()
    commented_by    =   models.ForeignKey(User)
    contented_at    =   models.DateTimeField(auto_now=True, auto_now_add=True)




