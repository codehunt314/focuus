from django.http import HttpResponseRedirect, HttpResponse
from django.shortcuts import render, render_to_response
from django.template import RequestContext
from django.utils.translation import ugettext, ugettext_lazy as _
from django.contrib.auth import get_user

from opinions.models import *
from datetime import datetime
import json, random, urllib2
from django.views.decorators.csrf import csrf_exempt
from news.models import *


def home(request):
    # print request.session.keys()
    logged_in   =   True if request.user.id else False
    # user        =   get_user(request) if logged_in else None
    # user_profile =  user.get_profile() if user else None
    # today       =   datetime.today()
    #all_articles  = Article.objects.filter(published_time__year=today.year, published_time__month=today.month, published_time__day=today.day)
    #all_articles  = Article.objects.all()[:20]
    #return render_to_response('home_bootstrap.html', {'all_articles': all_articles, 'logged_in': logged_in, 'user_profile': user_profile}, context_instance=RequestContext(request))
    all_articles  = [(article.title, article.url, article.source_website) for article in Article.objects.all()[:20]]
    return render_to_response('home_new1.html', {'logged_in': logged_in, 'all_articles': all_articles}, context_instance=RequestContext(request))
    # return render_to_response('home.html', {'logged_in': logged_in}, context_instance=RequestContext(request))
    # return render_to_response('home_basic.html', {'logged_in': logged_in}, context_instance=RequestContext(request))

def getarticle_ajax(request):
    page_num    =   int(request.GET.get('page', 0))
    # articles    =   Article.objects.order_by('?')[20*(page_num):20*(page_num+1)]
    articles    =   Article.objects.order_by('-published_time')[20*(page_num):20*(page_num+1)]
    articles_list = []
    for a in articles:
        articles_list.append({
                    'id': a.id, 'title': a.title, 'url': a.url, 'website': a.source.website, 
                    'vote': ['vipulshaily', 'richashaily'], 
                    'comment': [
                        {'text': 'comment', 'comment_by': 'vipulshaily', 'comment_at': '5 hr back'},
                        {'text': 'comment', 'comment_by': 'vipulshaily', 'comment_at': '5 hr back'}
                    ]
                })

    return HttpResponse(json.dumps(articles_list))

def get_articles(request):
    page_num        =   int(request.GET.get('page', 1))
    more_articles   =   Article.objects.all()[20*(page_num):20*(page_num+1)]
    article_tmplt   =   '<div class="article">\
                                <div class="block1">\
                                        <a href="#" class="upvote" data-id="%s"><div class="upvote_sign"></div></a>\
                                    <div class="votecount"><p>%s</p></div>\
                                </div>\
                                <div class="block2">\
                                    <div class="articletitle"><a href="%s">%s</a></div>\
                                    <div class="articlemeta">\
                                        <p><span>%s</span>&nbsp;&nbsp;&nbsp; by &nbsp;&nbsp;&nbsp;<span>Manish Chand</span></p>\
                                    </div>\
                                </div>\
                                <div class="block3"></div>\
                            </div>'
    articles = []
    for a in more_articles:
        articles.append(article_tmplt % (a.id, 34, a.url, a.title, a.source.website))
    return HttpResponse(json.dumps(articles))

@csrf_exempt
def upvote(request):
    if not request.user.id:
        return HttpResponse('login required')
    if request.method=='POST':
        articleid   =   request.POST.get('articleid')
        article     =   Article.objects.get(id=int(articleid))
        av, crtd    =   ArticleVote.objects.get_or_create(article=article, voted_by=request.user)
        if not crtd:
            av.delete()
        votecount   =   ArticleVote.objects.filter(article=article).count()
        return HttpResponse(str(votecount))
    return HttpResponse('wrong request type')

def comment(request, articleid):
    article = Article.objects.get(id=int(articleid))
    return HttpResponse('<a href="#x" class="overlay" id="comment"></a><div class="comment_popup">\
                    <p>%s</p>\
                    <form>\
                        <textarea>Comment</textarea>\
                    </form>\
                </div>' % (article.title))


"""
new section
"""


